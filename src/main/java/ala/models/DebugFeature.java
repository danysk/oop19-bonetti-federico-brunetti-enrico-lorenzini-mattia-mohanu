package ala.models;

public interface DebugFeature {

    /**
     * Method that calcolate FPS.
     * 
     */
    void calculateFPS();
}
