package ala.controllers;

public interface WalkingEnemiesAbilitiesController {
    /**
     * make the enemy move left and right to walk on a platform.
     * 
     */
    void moveEnemy();
}
