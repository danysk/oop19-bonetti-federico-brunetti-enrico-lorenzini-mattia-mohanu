package ala.controllers;

import ala.models.DynamicGameObjectModel;
import ala.views.DynamicGameObjectView;

/**
 * MoonController Class.
 * 
 */
public class MoonController extends DynamicGameObjectController {
    /**
     * Constructor.
     * 
     * @param dynamicGameObjectModel
     * @param dynamicGameObjectView
     * 
     */
    public MoonController(final DynamicGameObjectModel dynamicGameObjectModel, final DynamicGameObjectView dynamicGameObjectView) {
        super(dynamicGameObjectModel, dynamicGameObjectView);
    }

}
