package ala.controllers;

public interface ParadiseWalkingEnemiesAbilitiesController {
    /**
     * Method that makes paradise enemies attack when Lucifer enter in his range.
     * 
     */
    void walkingEnemyAttack();
}
